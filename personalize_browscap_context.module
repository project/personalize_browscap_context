<?php

/**
 * @file
 * Provides a visitor context plugin for targeting based on user agent.
 */

/**
 * Implements hook_help().
 */
function personalize_browscap_context_help($path, $arg) {
  switch ($path) {
    case 'admin/help#personalize_browscap_context':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Personalize Browscap Context module extends default context of <a href="@personalize-href">personalize</a> module by providing contexts based on <a href="@browscap-href">browscap</a>.', array('@personalize' => 'https://www.drupal.org/project/personalize', '@browscap-href' => 'https://www.drupal.org/project/browscap')) . '</p>';
      $output .= '<p>' . t('Go to <a href="@configuration-href">configuration</a> page to manage available options in the context.', array('@configuration-href' => url('admin/config/content/personalize/browscap'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function personalize_browscap_context_menu() {
  $items = array();
  $items['admin/config/content/personalize/browscap'] = array(
    'title' => 'Browscap',
    'description' => 'Configure loading user agent details to contexts.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('personalize_browscap_context_admin_form'),
    'access arguments' => array('administer personalize configuration'),
    'file' => 'personalize_browscap_context.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_page_build().
 */
function personalize_browscap_context_page_build(&$page) {
  $user_agent_properties = browscap_get_browser();
  $user_agent_params = personalize_browscap_context_get_user_agent_params();
  $personalize_browscap_context = array_intersect_key($user_agent_properties, $user_agent_params);

  $page['page_top']['personalize_browscap_context'] = array(
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'personalize_browscap_context') . '/js/personalize_browscap_context.js' => array(),
        array(
          'data' => array('personalize_browscap_context' => $personalize_browscap_context),
          'type' => 'setting',
        ),
      ),
    ),
  );
}

/**
 * Implements hook_personalize_visitor_context().
 */
function personalize_browscap_context_personalize_visitor_context() {
  $info = array();
  $path = drupal_get_path('module', 'personalize_browscap_context') . '/plugins';
  $info['browscap_context'] = array(
    'path' => $path . '/visitor_context',
    'handler' => array(
      'file' => 'BrowscapContext.inc',
      'class' => 'BrowscapContext',
    ),
  );

  return $info;
}

/**
 * Handler for loading available options for parameters from browscap.
 *
 * @return array
 *   Array of available options for each of parameter of user agent.
 */
function personalize_browscap_context_get_all_browscap_options() {
  // Check the cache for browscap context options.
  $cache = cache_get('personalize_browscap_context_options');

  // Attempt to find a cached browscap context options.
  // Otherwise store the browscap context options data in the cache.
  if (!empty($cache) && ($cache->created > REQUEST_TIME - 60 * 60 * 24)) {
    $browscap_context_options = $cache->data;
  }
  else {
    // Get user agent params with titles.
    $browscap_params = personalize_browscap_context_get_user_agent_params();

    // Get machine names of user agent params.
    $params = array_keys($browscap_params);

    // Load all of records from browscap.
    $query_result = db_select('browscap', 'b')
      ->fields('b', array('data'))
      ->execute();

    // Fetch options and group by user agent params.
    $browscap_context_options = array();
    foreach ($query_result as $row) {
      foreach ($params as $param) {
        if (!isset($browscap_context_options[$param])) {
          $browscap_context_options[$param] = array();
        }

        if (!empty($row->data) && $data = unserialize($row->data)) {
          if (!empty($data[$param]) && ($option = $data[$param]) && empty($browscap_context_options[$param][$option])) {
            $browscap_context_options[$param][$option] = $option;
          };
        }
      }
    }
    cache_set('personalize_browscap_context_options', $browscap_context_options);
  }

  return $browscap_context_options;
}

/**
 * Return available user agent parameters.
 */
function personalize_browscap_context_get_user_agent_params() {
  $params = array(
    'browser' => t('Browser'),
    'platform' => t('Platform'),
    'ismobiledevice' => t('Is Mobile Device'),
    'istablet' => t('Is Tablet'),
    'device_type' => t('Device Type'),
  );

  return $params;
}

/**
 * Return browsers available for context.
 */
function _personalize_browscap_context_get_fields_contexts_options() {
  $personalize_browscap_context = variable_get('personalize_browscap_context', array());
  $browscap_options = personalize_browscap_context_get_all_browscap_options();
  $fields_contexts_options = array();
  foreach ($browscap_options as $param => $browscap_param_options) {
    $contexts_options = array();
    switch ($param) {
      case 'browser':
      case 'platform':
      case 'device_type':
        if (!empty($personalize_browscap_context[$param])) {
          $context_settings = $personalize_browscap_context[$param];
          if ($context_settings['visibility'] === '0') {
            $contexts_options = array_diff($browscap_param_options, $context_settings['list']);
          }
          else {
            $contexts_options = array_combine($context_settings['list'], $context_settings['list']);
          }
        }
        else {
          $contexts_options = $browscap_param_options;
        }
        break;

      case 'ismobiledevice':
      case 'istablet':
        $contexts_options = array(
          'true' => t('Yes'),
          'false' => t('No'),
        );
        break;
    }

    $fields_contexts_options[$param] = $contexts_options;
  }

  return $fields_contexts_options;
}
